module gitlab.com/uglvdhigr29nb0fwchmgtkftqsak

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1
	github.com/vrischmann/envconfig v1.3.0
	k8s.io/apimachinery v0.21.0
)
