Run on your local machine:
1. clone the repository
2. fetch dependencies 
```bash
> go mod vendor
```
3. run server
```bash
> go run ./cmd/main.go
```
4. in separate window check server with default parameters
```bash
> curl -i http://localhost:8080/pictures
> curl -i http://localhost:8080/pictures\?start_date\=2020-01-04\&end_date\=2020-01-05
```

Run as a docker image:
```bash
> docker build -t server .
> docker run -d -p 8080:8080 server:latest
```
