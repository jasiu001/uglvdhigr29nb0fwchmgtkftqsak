package urls

import (
	"sync"
	"time"

	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/parameter"

	"github.com/pkg/errors"
)

//go:generate mockery -name=ImageFetcher -output=automock -outpkg=automock -case=underscore
type ImageFetcher interface {
	FetchURL(string) (string, error)
}

type Manager struct {
	guard         LimitGuard
	workersAmount int
	waitGroup     sync.WaitGroup
	images        ImageFetcher
}

func NewManager(fetcher ImageFetcher, conRqs int) *Manager {
	return &Manager{
		guard: LimitGuard{
			// according to the https://api.nasa.gov/ doc limit for API_KEY is 1000 per hour
			// due to only one API (NASA), guard is created in Manager constructor
			hourLimit: 1000,
			keyGiver: func() string {
				return time.Now().Format("2006-01-02 15")
			},
			mutex: sync.Mutex{},
		},
		images:        fetcher,
		workersAmount: conRqs,
		waitGroup:     sync.WaitGroup{},
	}
}

// FetchURLs fetches URLs from images API based on parameters from argument, before it checks if limit for
// images API is not reached
func (m *Manager) FetchURLs(bag parameter.Donor) ([]string, error) {
	data := make(map[string]string, 0)

	for d := bag.StartDate(); bag.EndDate().Add(24 * time.Hour).After(d); d = d.AddDate(0, 0, 1) {
		data[d.Format("2006-01-02")] = ""
	}

	// check that the number of elements is not higher than the current allowed image API limit
	if m.guard.IsLimitReached(len(data)) {
		return nil, errors.New("limit for image API reached")
	}

	var fetchError error
	chunk := chunkBy(keysFromMap(data), m.workersAmount)
	for _, datesRange := range chunk {
		dr := datesRange
		for _, value := range dr {
			m.waitGroup.Add(1)
			v := value
			go func() {
				url, err := m.images.FetchURL(v)
				if err != nil {
					fetchError = errors.Wrap(err, "while fetching URL from image API")
					return
				}
				data[v] = url
				m.waitGroup.Done()
			}()
		}
		m.waitGroup.Wait()
		if fetchError != nil {
			return nil, fetchError
		}
	}

	return valuesFromMap(data), nil
}

func chunkBy(items []string, chunkSize int) (chunks [][]string) {
	for chunkSize < len(items) {
		items, chunks = items[chunkSize:], append(chunks, items[0:chunkSize:chunkSize])
	}

	return append(chunks, items)
}

func valuesFromMap(m map[string]string) []string {
	v := make([]string, 0, len(m))

	for _, value := range m {
		v = append(v, value)
	}

	return v
}

func keysFromMap(m map[string]string) []string {
	k := make([]string, 0, len(m))

	for key, _ := range m {
		k = append(k, key)
	}

	return k
}
