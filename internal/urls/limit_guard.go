package urls

import "sync"

type LimitGuard struct {
	mutex          sync.Mutex
	hourLimit      int
	currentHour    string
	requestsAmount int
	keyGiver       func() string
}

// IsLimitReached checks if number of elements is higher than hour limit
func (lg *LimitGuard) IsLimitReached(amount int) bool {
	lg.mutex.Lock()
	defer lg.mutex.Unlock()

	// amount is higher than all limit, return immediately
	if amount > lg.hourLimit {
		return true
	}

	n := lg.keyGiver()
	// if limit was not check for current hour, create limit and add amount
	if lg.currentHour != n {
		lg.currentHour = n
		lg.requestsAmount = amount
		return false
	}

	// if limit for current hour exist add amount and check if exceeded the limit
	// if not add amount and return
	if lg.requestsAmount+amount > lg.hourLimit {
		return true
	}
	lg.requestsAmount = lg.requestsAmount + amount

	return false
}
