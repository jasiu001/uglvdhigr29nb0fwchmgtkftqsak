package urls

import (
	"testing"
	"time"

	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/urls/automock"

	"github.com/stretchr/testify/require"
)

func TestManager_FetchURLs(t *testing.T) {
	// given
	bag := testBag{t: t}

	imageFetcher := &automock.ImageFetcher{}
	imageFetcher.On("FetchURL", "2020-04-10").Return("https://one.com", nil)
	imageFetcher.On("FetchURL", "2020-04-11").Return("https://five.com", nil)
	imageFetcher.On("FetchURL", "2020-04-12").Return("https://two.com", nil)
	imageFetcher.On("FetchURL", "2020-04-13").Return("https://four.com", nil)
	imageFetcher.On("FetchURL", "2020-04-14").Return("https://three.com", nil)
	defer imageFetcher.AssertExpectations(t)

	m := NewManager(imageFetcher, 2)

	// when
	urls, err := m.FetchURLs(bag)

	// then
	require.NoError(t, err)
	require.Len(t, urls, 5)
	require.Contains(t, urls, "https://one.com")
	require.Contains(t, urls, "https://two.com")
	require.Contains(t, urls, "https://three.com")
	require.Contains(t, urls, "https://four.com")
	require.Contains(t, urls, "https://five.com")
}

type testBag struct {
	t *testing.T
}

func (t testBag) StartDate() time.Time {
	r, err := time.Parse("2006-01-02", "2020-04-10")
	if err != nil {
		t.t.Fatal("cannot parse start date")
	}
	return r
}

func (t testBag) EndDate() time.Time {
	r, err := time.Parse("2006-01-02", "2020-04-14")
	if err != nil {
		t.t.Fatal("cannot parse end date")
	}
	return r
}
