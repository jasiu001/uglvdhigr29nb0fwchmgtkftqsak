package urls

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestLimitGuard_IsLimitReached(t *testing.T) {
	// given
	test := 0
	lg := LimitGuard{
		hourLimit: 20,
		keyGiver: func() string {
			test++
			if test <= 3 {
				return time.Now().Format("2006-01-02 15")
			}
			return time.Now().Add(1 * time.Hour).Format("2006-01-02 15")
		},
	}

	// when/then
	limitAchieved := lg.IsLimitReached(10)
	require.False(t, limitAchieved)

	limitAchieved = lg.IsLimitReached(5)
	require.False(t, limitAchieved)

	limitAchieved = lg.IsLimitReached(6)
	require.True(t, limitAchieved)

	limitAchieved = lg.IsLimitReached(5)
	require.False(t, limitAchieved)

	limitAchieved = lg.IsLimitReached(16)
	require.True(t, limitAchieved)
}
