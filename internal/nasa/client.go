package nasa

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"k8s.io/apimachinery/pkg/util/wait"
)

const URLTemplate = "https://api.nasa.gov/planetary/apod?api_key=%s&date=%s"

type Client struct {
	ApiKey string
}

type Response struct {
	Date string `json:"date"`
	URL  string `json:"url"`
}

func NewClient(apiKey string) *Client {
	return &Client{ApiKey: apiKey}
}

// FetchURL sends request for NASA API with parameters to fetch URL from response
func (c *Client) FetchURL(date string) (string, error) {
	r := Response{}

	err := wait.PollImmediate(500*time.Millisecond, 2*time.Second, func() (done bool, err error) {
		response, err := http.Get(fmt.Sprintf(URLTemplate, c.ApiKey, date))
		if err != nil {
			return false, errors.Wrap(err, "while send request to server")
		}
		defer response.Body.Close()

		// try send request again if server returns >= 500
		if response.StatusCode >= http.StatusInternalServerError {
			return false, nil
		}
		err = json.NewDecoder(response.Body).Decode(&r)
		if err != nil {
			return false, errors.Wrap(err, "while decoding response from server")
		}

		return true, nil
	})

	if err != nil {
		return "", errors.Wrap(err, "while getting URL from NASA API")
	}

	return r.URL, nil
}
