package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/parameter"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type URLFetcher interface {
	FetchURLs(parameter.Donor) ([]string, error)
}

type (
	ErrorResponse struct {
		Error string `json:"error"`
	}

	PictureResponse struct {
		URLs []string `json:"urls"`
	}
)

type Server struct {
	URLManager URLFetcher
	log        logrus.FieldLogger
}

func New(manager URLFetcher, log logrus.FieldLogger) *Server {
	return &Server{
		URLManager: manager,
		log:        log,
	}
}

// AttachRoutes connects endpoints with router
func (s *Server) AttachRoutes(router *mux.Router) {
	router.HandleFunc("/pictures", s.pictures).Methods(http.MethodGet)
}

// pictures HTTP handler which validates user request, fetches URLs and returns those URLs as a JSON
func (s *Server) pictures(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	pb := &parameter.Bag{
		Start: r.URL.Query().Get("start_date"),
		End:   r.URL.Query().Get("end_date"),
	}
	err := pb.ValidateAndSave()
	if err != nil {
		s.log.Errorf("incorrect query parameters: %s", err)
		s.handleError(w, http.StatusBadRequest, "incorrect query parameters")
		return
	}

	urls, err := s.URLManager.FetchURLs(pb)
	if err != nil {
		s.log.Errorf("cannot fetch URLs: %s", err)
		s.handleError(w, http.StatusInternalServerError, "cannot fetch URLs")
		return
	}

	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(PictureResponse{URLs: urls})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (s *Server) handleError(w http.ResponseWriter, code int, errMsg string) {
	w.WriteHeader(code)
	err := json.NewEncoder(w).Encode(ErrorResponse{Error: errMsg})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}
