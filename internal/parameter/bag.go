package parameter

import (
	"time"

	"github.com/pkg/errors"
)

type Donor interface {
	StartDate() time.Time
	EndDate() time.Time
}

type Bag struct {
	from  time.Time
	to    time.Time
	Start string
	End   string
}

// ValidateAndSave validates passed strings and saves them as correct type parameter
func (b *Bag) ValidateAndSave() error {
	sd, err := time.Parse("2006-01-02", b.Start)
	if err != nil {
		return errors.Wrap(err, "start date parameters is incorrect")
	}

	ed, err := time.Parse("2006-01-02", b.End)
	if err != nil {
		return errors.Wrap(err, "end date parameters is incorrect")
	}

	if sd.After(ed) {
		return errors.New("start date parameters is incorrect: start date is after end date")
	}

	b.from = sd
	b.to = ed

	return nil
}

// StartDate returns start date parameter
func (b *Bag) StartDate() time.Time {
	return b.from
}

// EndDate returns end date parameter
func (b *Bag) EndDate() time.Time {
	return b.to
}
