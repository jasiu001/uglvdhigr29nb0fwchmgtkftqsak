package parameter

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestBag_Validate(t *testing.T) {
	for name, tc := range map[string]struct {
		startDate    string
		endDate      string
		shouldError  bool
		errorMessage string
	}{
		"dates are correct": {
			startDate:    "2020-01-04",
			endDate:      "2020-02-05",
			shouldError:  false,
			errorMessage: "",
		},
		"dates are equal": {
			startDate:    "2020-01-04",
			endDate:      "2020-01-04",
			shouldError:  false,
			errorMessage: "",
		},
		"dates have incorrect format": {
			startDate:    "04/01/2020",
			endDate:      "05/02/2020",
			shouldError:  true,
			errorMessage: `start date parameters is incorrect: parsing time "04/01/2020" as "2006-01-02": cannot parse "1/2020" as "2006"`,
		},
		"dates are empty": {
			startDate:    "",
			endDate:      "",
			shouldError:  true,
			errorMessage: `start date parameters is incorrect: parsing time "" as "2006-01-02": cannot parse "" as "2006"`,
		},
		"start date is higher than end date": {
			startDate:    "2020-02-05",
			endDate:      "2020-01-04",
			shouldError:  true,
			errorMessage: "start date parameters is incorrect: start date is after end date",
		},
	} {
		t.Run(name, func(t *testing.T) {
			pb := Bag{
				Start: tc.startDate,
				End:   tc.endDate,
			}
			err := pb.ValidateAndSave()

			if tc.shouldError {
				require.Error(t, err)
				require.Equal(t, tc.errorMessage, err.Error())
			} else {
				require.NoError(t, err)

				sd, err := time.Parse("2006-01-02", tc.startDate)
				require.NoError(t, err)
				require.Equal(t, sd, pb.StartDate())

				ed, err := time.Parse("2006-01-02", tc.endDate)
				require.NoError(t, err)
				require.Equal(t, ed, pb.EndDate())
			}
		})
	}
}
