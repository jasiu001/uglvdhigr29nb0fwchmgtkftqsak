FROM golang:1.16.3-alpine

ARG BASE_APP_DIR=/go/src/gitlab.com/uglvdhigr29nb0fwchmgtkftqsak

WORKDIR $BASE_APP_DIR
COPY ./go.mod ${BASE_APP_DIR}/go.mod
COPY ./go.sum ${BASE_APP_DIR}/go.sum
COPY ./cmd ${BASE_APP_DIR}/cmd
COPY ./internal ${BASE_APP_DIR}/internal

RUN apk add git && go mod vendor
RUN CGO_ENABLED=0 GOOS=linux go build -o server ${BASE_APP_DIR}/cmd/
RUN mkdir /app && mv ${BASE_APP_DIR}/server /app/server

CMD ["/app/server"]
