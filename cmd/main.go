package main

import (
	"fmt"
	"net/http"

	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/nasa"
	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/server"
	"gitlab.com/uglvdhigr29nb0fwchmgtkftqsak/internal/urls"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

type Config struct {
	ApiKey             string `envconfig:"default=DEMO_KEY"`
	ConcurrentRequests int    `envconfig:"default=5"`
	Port               string `envconfig:"default=8080"`
}

func main() {
	var cfg Config
	err := envconfig.Init(&cfg)
	fatalOnError(err)

	log := logrus.New()

	cli := nasa.NewClient(cfg.ApiKey)
	urlManager := urls.NewManager(cli, cfg.ConcurrentRequests)

	router := mux.NewRouter()
	srv := server.New(urlManager, log.WithField("service", "server"))
	srv.AttachRoutes(router)

	fatalOnError(http.ListenAndServe(fmt.Sprintf(":%s", cfg.Port), router))
}

func fatalOnError(err error) {
	if err != nil {
		logrus.Fatal(err)
	}
}
